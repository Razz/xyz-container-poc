# Ping Pong Demo App

Welcome to the ping-pong demo app! This is a gin service that will respond with a message when a `GET` request is sent to `/healthz` or `/ping`

# Quick Guide

## How to Deploy Quickly

ping-pong utilizes `helm` to deploy, to make this easy run `make _deploy` to deploy just the helm chart into your cluster under the `main` namespace

## Testing

We will utilize Kubernetes port-forwarding to test the ping-pong service, this can be done easily with the command `make _np_test`. Afterwords, in another terminal window, run `curl localhost:8080/healthz` or in a browser window go to `localhost:8080/healthz` to see the message!

after running `make _test` or `make _np_test` run `make _stop` to kill kubectl and it's port-forwarding

Note: the command `_np_test` is short for "no push test", this will not push a new container to k8s, so all of the changes that are local will not be reflected in the test

# Dev

Developing on the ping-pong service is straight forward and we will be utilizing `make` and `docker-compose` for local development

## Getting Started

To get started, run `make dev`. What this will do is compile your changes to the service into a local docker image (`ping-pong:local`) and run `docker-compose up`. Too interact with the ping-pong service, you can `curl localhost:8080/healthz`. 

## Making changes locally
If you want to add or modify the service, make changes to [main.go](src/ping-pong/main.go) and then run `make dev`

## Making changes to your namespace
We have provided a few mechanisms to do this. The easiest way to achieve this is to push your local branch to git and have Gitlab-CI create your container, and push your container to your branches namespace. After the pipeline is complete, you can run `make _test` to set up port forwarding, and run curl (see above)

Alternatively, you can run `make deploy` to build your container locally, push it to gitlab, and deploy to Kubernetes.

Note: This will require logging in to Gitlab and having access to the repo as a Developer to do this.

Note: If `make build` fails, you may need to run the command again with `sudo`

## Merging

Once your done making changes and want to commit them to main, open up a PR, this will re run the CI pipeline and push your changes to your namespace for review by the team. Once this merge is complete run the `cleanup` job found in the pipeline by hitting the play button. This will delete your branch namespace, and all related resources.


# Explainer on the  basic make commands:
- `make dev` will build a local container for the ping-pong service, and start docker-compose 
- `make login` will log you into the gitlab docker registry enabling you to push containers
- `make build` will build the ping-pong docker container locally under the tag `ping-pong:local`
- `make push` will tag `ping-pong:local` to `registry.gitlab.com/razz/xyz-container-poc/ping-pong:$YOUR_BRANCH` and push it to gitlab
- `make deploy` will deploy the ping-pong helm chart to your branch's namespace, as well as create it for you is it doesn't already exist
- `make test` will open up a port-forward connect to your branch's ping-pong service, allowing you to interact with it on `localhost:8080`, as well as curl the endpoint on your behalf
- `make _dev` will just run `docker-compose up`
- `make _push` will just re-tag the local docker container and push it to the git lab registry
- `make _deploy` will just run the helm upgrade
- `make _test` will just do the port-forwarding to Kubernetes in the background and curl to test
- `make _stop` will find and kill the background kubectl pid