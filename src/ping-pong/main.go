package main

import (
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

var router = gin.Default()

type Message = map[string]any

func getTimeMessage() Message {
	m := Message{
		"message":   "Automate all the things!!",
		"timestamp": time.Now().Unix(),
	}
	return m
}

func main() {

	config := viper.New()
	config.SetDefault("port", "8080")
	config.AutomaticEnv()

	router.GET("/healthz", func(c *gin.Context) {
		c.JSON(200, getTimeMessage())
	})
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(200, "pong")
	})
	router.GET("/hello", func(c *gin.Context) {
		c.JSON(200, "Hello Everyone!")
	})

	router.Run(":" + config.GetString("port"))
}
