.PHONY: help build _deploy deploy build _dev dev _push push _test test

BRANCH = $(shell git rev-parse --abbrev-ref HEAD)


login:
	docker login registry.gitlab.com

build:
	docker build -t ping-pong:local -f src/ping-pong/Dockerfile src/ping-pong/

_dev: 
	docker-compose up

dev: build _dev


_push:
	docker tag ping-pong:local registry.gitlab.com/razz/xyz-container-poc/ping-pong:$(BRANCH)
	docker push registry.gitlab.com/razz/xyz-container-poc/ping-pong:$(BRANCH)


push: login build _push


_namespace:
	-kubectl create namespace $(BRANCH)

_deploy:
	helm upgrade --install --atomic -f helm/ping-pong/dev.yaml -n $(BRANCH) ping-pong helm/ping-pong/ --set image.tag=$(BRANCH)

deploy: push _namespace _deploy


_test:
	kubectl port-forward deployment/ping-pong 8080:80 -n $(BRANCH) & 
	sleep 5
	curl localhost:8080/healthz

_np_test: _deploy _test

_stop:
	ps aux | grep -i kubectl | grep -v grep | awk {'print $$2'} | xargs kill

test: deploy _test _stop
