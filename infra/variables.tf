variable "region" {
  type        = string
  default     = "us-east-1"
  description = "AWS Region for Provider"
}

variable "aws_profile" {
  type        = string
  default     = "default"
  description = "AWS Profile to use"
}

# NETWORK

variable "vpc_name" {
  type        = string
  description = "Name to be used on all the resources as identifier"
  default     = "eks-dev"
}

variable "public_subnets" {
  type        = list(string)
  description = "A list of public subnets inside the VPC"
  default     = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
}
variable "private_subnets" {
  type        = list(string)
  description = "A list of private subnets inside the VPC"
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "azs" {
  type        = list(string)
  description = "A list of availability zones specified as argument to this module"
  default     = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "enable_nat_gateway" {
  type        = bool
  description = "Should be true if you want to provision NAT Gateways for each of your private networks"
  default     = "true"
}

variable "enable_dns_hostnames" {
  type        = bool
  description = "Should be true to enable DNS hostnames in the VPC"
  default     = "true"
}

variable "enable_dns_support" {
  type        = bool
  description = "Should be true to enable DNS support in the VPC"
  default     = "true"
}

variable "single_nat_gateway" {
    type = bool
    description = "Create a single NAT Gateway, set false if one nat per az"
    default = "true"
}

variable "egress_only_igw" {
    type = bool
    description = "Create an Egress only IGW, set false if being on the internet is bad"
    default = "true"
}

variable "vpc_tags" {
  type = map(string)
  default = {
    Environment = "dev"
  }
}

# EKS

variable "cluster_name" {
  type        = string
  description = "Name of the EKS cluster"
  default     = "dev-eks"
}

variable "cluster_endpoint_private_access" {
  type        = bool
  description = "Indicates whether or not the Amazon EKS private API server endpoint is enabled"
  default     = "true"
}

variable "cluster_endpoint_public_access" {
  type        = bool
  description = "Indicates whether or not the Amazon EKS public API server endpoint is enabled"
  default     = "false"
}

variable "eks_tags" {
  type = map(string)
  default = {
    Environment = "dev"
  }
}