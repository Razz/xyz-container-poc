data "aws_iam_group" "eks_owners" {
  group_name = "eks-admins"
}



module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 19.17.2"

  cluster_name    = var.cluster_name
  cluster_version = "1.28"

  cluster_endpoint_private_access = var.cluster_endpoint_private_access
  cluster_endpoint_public_access  = var.cluster_endpoint_public_access

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets

  kms_key_administrators = data.aws_iam_group.eks_owners.users.*.arn

  cluster_addons = {
    coredns = {
      most_recent = true
    }
    kube-proxy = {
      most_recent = true
    }
    vpc-cni = {
      most_recent = true
    }
  }

  eks_managed_node_group_defaults = {
    disk_size = 30
  }

  eks_managed_node_groups = {
    general = {
      desired_size = 1
      min_size     = 1
      max_size     = 3

      labels = {
        role = "general"
      }

      instance_types = ["t3.small"]
      capacity_type  = "ON_DEMAND"
    }
  }

  manage_aws_auth_configmap = true
  aws_auth_users = [
    for _, user in data.aws_iam_group.eks_owners.users:
      {
        userarn  = user.arn
        username =  user.user_name
        groups   = ["system:masters"]
      }
  ]

  tags = var.eks_tags
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_name
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_name
}


## TODO: Add the KMS and other cluster specific IAM polices
##       added to a group so that we dont run into the 
##       KMS key policy issue this module creates         