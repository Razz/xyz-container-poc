# XYZ Kube PoC

## Demo
In this PoC we want to demo the following capabilities:
 - Containerizing a simple http service (via docker)
 - Running Containers locally (via make and docker-compose)
 - Deploying a service to k8s (via helm)
    - Remote Development in namespaces
    - Deploying to main in a prod namespace
- Integration testing in Merge
- No downtime deploys
- CI/CD for services, containers, and deployments
- 

---

## User Stories

### Guiding User Story

As a developer I would like to be able to interact with, modify, and deploy a simple service to k8s, so that I can better understand how a containerized development workflow might fit here at XYZ.

---
### User Story Breakdown
- As a developer, I need to be able to spin up an EKS cluster, so that I can experiment with k8s
- As a developer, I need an http service that returns a dynamic response, so that I have a baseline to start with
- As a developer, I need to have an example of a service in a docker container, so that I can interact with that service in isolation
- As a developer, I would like to be able to build my services and containers via make, so that I can easily iterate locally 
- As a developer, I would like to have the ability to bring up this service via docker-compose, so that in the future I can work on multiple services locally
- As a developer, I need to have a container repository, so that I can push pull base containers and service containers
- As a developer, I would like to have service containers built and deployed to the container repo via CI, so that I can ensure constancy 
- As a developer, I need a helm chart that lays out my deployment, so that I can deploy my service and deps without needing to understand kubectl
-  As a developer I would like to see deployments from both helm, as well as the helm provider in terraform, so that I can see what works best for me

---

### Reach goals as stories

- As a developer I would like to see how multiple services, one complied and one interpreted, are deployed together, so that I have a better example of what this workflow looks like outside of this PoC
- As a developer, I would like to see a database also deployed and interacted with, so that I have a better reference for how stateful applications work in k8s
- As a developer, I would like to see how configuration via environment variables work, including with secrets, so that I can better see how to migrate my existing apps to this workflow
- As a developer, I would like my remote development to take place in namespaces, so that we only have to interact with a single k8s cluster
- As a developer, I would like to see alternative ways to deploy all or part of my application outside of k8s, so that I can further explore how my team might choose to move to containerized workloads
---

## Plan for MVP
- [x] Create an S3 bucket for remote state
- [x] Build or find a tf module to spin up an EKS cluster
- [x] Create a Gitlab IAM user, tie to this repo
- [x] Build a flask app, (or maybe gin?) that serves ```{
  “message”: “Automate all the things!”,
  “timestamp”: 1529729125
}``` json on `/healthz`
- [x] Containerize said app
- [ ] Create a Makefile for testing, building, and running locally
- [x] Create a helm chart for this service
- [x] Set up CI for Service
- [x] Set up CD for Service
- [ ] Update this README for running, updating, deploying, etc. this application
- [ ] Reassess, plan for reach goals time permitting.  